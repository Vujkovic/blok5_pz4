﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ4.Model
{
    public class NetworkModel
    {
        private List<SubstationEntity> substations;
        private List<NodeEntity> nodes;
        private List<SwitchEntity> switches;
        private List<LineEntity> lines;

        public List<SubstationEntity> Substations { get => substations; set => substations = value; }
        public List<NodeEntity> Nodes { get => nodes; set => nodes = value; }
        public List<SwitchEntity> Switches { get => switches; set => switches = value; }
        public List<LineEntity> Lines { get => lines; set => lines = value; }
    }
}
