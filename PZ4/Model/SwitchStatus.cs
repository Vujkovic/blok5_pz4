﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ4.Model
{
    public enum SwitchStatus { Open, Closed }
    public class SwitchEntity : Entity
    {
        double x;
        double y;
        SwitchStatus status;

        public SwitchEntity()
        {
        }

        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }
        public SwitchStatus Status { get => status; set => status = value; }
    }
}
