﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ4.Model
{
    public class SubstationEntity : Entity
    {
        double x;
        double y;

        public SubstationEntity()
        {
        }

        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }
    }
}
