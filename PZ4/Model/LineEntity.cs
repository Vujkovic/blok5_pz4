﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PZ4.Model
{
    public class LineEntity : Entity
    {
        bool isUnderground;
        double r;
        string conductorMaterial;
        string lineType;
        int thermalConstantHeat;
        String firstEnd;
        String secondEnd;
        List<Point> vertices = new List<Point>();

        public LineEntity()
        {
        }

        public bool IsUnderground { get => isUnderground; set => isUnderground = value; }
        public double R { get => r; set => r = value; }
        public string ConductorMaterial { get => conductorMaterial; set => conductorMaterial = value; }
        public string LineType { get => lineType; set => lineType = value; }
        public int ThermalConstantHeat { get => thermalConstantHeat; set => thermalConstantHeat = value; }
        public String FirstEnd { get => firstEnd; set => firstEnd = value; }
        public String SecondEnd { get => secondEnd; set => secondEnd = value; }
        public List<Point> Vertices { get => vertices; set => vertices = value; }
    }
}
