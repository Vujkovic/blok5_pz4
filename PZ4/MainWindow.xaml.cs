﻿using PZ4.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace PZ4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private NetworkModel NetworkModel;

        public double TopRightLat = 45.277031;
        public double TopRightLon = 19.894459;
        public double BottomLeftLat = 45.2325;
        public double BottomLeftLon = 19.793909;

        private Point start = new Point();
        private Point diffOffset = new Point();
        private Point startPosition = new Point();
        public Point curMouse = new Point();

        private GeometryModel3D hitgeo;

        private int zoomMax = 100;
        private int zoomCurent = 1;

        public List<GeometryModel3D> drawnSubstations = new List<GeometryModel3D>();
        public List<GeometryModel3D> drawnNodes = new List<GeometryModel3D>();
        public List<GeometryModel3D> drawnSwitches = new List<GeometryModel3D>();

        public Dictionary<GeometryModel3D, string> models = new Dictionary<GeometryModel3D, string>();

        public static List<double> allLatitudes = new List<double>();
        public static List<double> allLongitudes = new List<double>();

        public static int[,] MainGrid;  //matrica grida 
        public double LongStep; // korak po longitudi
        public double LatitStep; // korak po latitudi

        //List<Tuple<string, LineEntity>> dicL = new List<Tuple<string, LineEntity>>();
        //List<Tuple<string, SubstationEntity>> dicSS = new List<Tuple<string, SubstationEntity>>();
        //List<Tuple<string, SwitchEntity>> dicSW = new List<Tuple<string, SwitchEntity>>();
        //List<Tuple<string, NodeEntity>> dicN = new List<Tuple<string, NodeEntity>>();
        List<SubstationEntity> SSL = new List<SubstationEntity>();
        List<SwitchEntity> SWL = new List<SwitchEntity>();
        List<NodeEntity> NL = new List<NodeEntity>();
        List<Entity> EL = new List<Entity>();

        public MainWindow()
        {
            InitializeComponent();
            NetworkModel = new NetworkModel();
            try
            {
                XmlSerializer serializer = new XmlSerializer(NetworkModel.GetType());
                using (FileStream reader = new FileStream("Geographic.xml", FileMode.Open))
                {
                    NetworkModel = (NetworkModel)serializer.Deserialize(reader);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                MessageBox.Show("Error reading Geographic.xml");
                this.Close();
            }
            GetLatLon();
            DrawNodes();
            DrawSwitches();
            DrawSubstations();
            DrawLines();
           
        }


        public void DrawNodes()
        {
            double sLat;
            double sLon;
            Double x;
            Double y;
            double offsetLat = TopRightLat - BottomLeftLat;
            double offsetLon = TopRightLon - BottomLeftLon;

            foreach (var n in NL)
            {
                ToLatLon(n.X, n.Y, out sLat, out sLon);

                if ((sLat > BottomLeftLat && sLat < TopRightLat) && (sLon < TopRightLon && sLon > BottomLeftLon))
                {
                    y = -15 + (((sLat - BottomLeftLat) / offsetLat) * 30);
                    x = -15 + (((sLon - BottomLeftLon) / offsetLon) * 30);

                    GeometryModel3D model = new GeometryModel3D();
                    model.Geometry = new MeshGeometry3D();
                    model.Material = new DiffuseMaterial(Brushes.Chocolate);
                    MeshGeometry3D geometry = new MeshGeometry3D();

                    geometry.Positions.Add(new Point3D(x - 0.07, y - 0.07, 0.1));
                    geometry.Positions.Add(new Point3D(x - 0.07, y + 0.07, 0.1));
                    geometry.Positions.Add(new Point3D(x + 0.07, y - 0.07, 0.1));
                    geometry.Positions.Add(new Point3D(x + 0.07, y + 0.07, 0.1));
                    geometry.Positions.Add(new Point3D(x - 0.07, y - 0.07, 0.2));
                    geometry.Positions.Add(new Point3D(x - 0.07, y + 0.07, 0.2));
                    geometry.Positions.Add(new Point3D(x + 0.07, y - 0.07, 0.2));
                    geometry.Positions.Add(new Point3D(x + 0.07, y + 0.07, 0.2));

                    geometry.TriangleIndices.Add(6);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(7);

                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(3);

                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(3);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(5);
                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(6);
                    geometry.TriangleIndices.Add(7);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(5);

                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(1);
                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(5);

                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(1);
                    geometry.TriangleIndices.Add(3);

                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(2);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(6);

                    model.Geometry = geometry;
                    myModel.Children.Add(model);
                    drawnNodes.Add(model);
                    models.Add(model, String.Format("ID: {0}\nNAME: {1}\nTYPE: NODE", n.Id, n.Name));
                }
            }
        }

        public void DrawSwitches()
        {
            double sLat;
            double sLon;
            Double x;
            Double y;
            double offsetLat = TopRightLat - BottomLeftLat;
            double offsetLon = TopRightLon - BottomLeftLon;

            foreach (var sw in SWL)
            {
                ToLatLon(sw.X, sw.Y, out sLat, out sLon);

                if ((sLat > BottomLeftLat && sLat < TopRightLat) && (sLon < TopRightLon && sLon > BottomLeftLon))
                {
                    y = -15 + (((sLat - BottomLeftLat) / offsetLat) * 30);
                    x = -15 + (((sLon - BottomLeftLon) / offsetLon) * 30);

                    GeometryModel3D model = new GeometryModel3D();
                    model.Geometry = new MeshGeometry3D();
                    model.Material = new DiffuseMaterial(Brushes.Teal);
                    MeshGeometry3D geometry = new MeshGeometry3D();

                    geometry.Positions.Add(new Point3D(x - 0.05, y - 0.05, 0.1));
                    geometry.Positions.Add(new Point3D(x - 0.05, y + 0.05, 0.1));
                    geometry.Positions.Add(new Point3D(x + 0.05, y - 0.05, 0.1));
                    geometry.Positions.Add(new Point3D(x + 0.05, y + 0.05, 0.1));
                    geometry.Positions.Add(new Point3D(x - 0.05, y - 0.05, 0.2));
                    geometry.Positions.Add(new Point3D(x - 0.05, y + 0.05, 0.2));
                    geometry.Positions.Add(new Point3D(x + 0.05, y - 0.05, 0.2));
                    geometry.Positions.Add(new Point3D(x + 0.05, y + 0.05, 0.2));

                    geometry.TriangleIndices.Add(6);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(7);

                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(3);

                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(3);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(5);
                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(6);
                    geometry.TriangleIndices.Add(7);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(5);

                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(1);
                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(5);

                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(1);
                    geometry.TriangleIndices.Add(3);

                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(2);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(6);

                    model.Geometry = geometry;
                    myModel.Children.Add(model);
                    drawnSwitches.Add(model);
                    models.Add(model, String.Format("ID: {0}\nNAME: {1}\nTYPE: SWITCH", sw.Id, sw.Name));
                }
            }
        }

        public void DrawSubstations()
        {
            double sLat = 0;
            double sLon = 0;
            double x = 0;
            double y = 0;

            double offsetLat = TopRightLat - BottomLeftLat;
            double offsetLon = TopRightLon - BottomLeftLon;

            foreach (var s in SSL)
            {
                ToLatLon(s.X, s.Y, out sLat, out sLon);

                if ((sLat > BottomLeftLat && sLat < TopRightLat) && (sLon < TopRightLon && sLon > BottomLeftLon))
                {
                    y = -15 + (((sLat - BottomLeftLat) / offsetLat) * 30);
                    x = -15 + (((sLon - BottomLeftLon) / offsetLon) * 30);

                    GeometryModel3D model = new GeometryModel3D
                    {
                        Geometry = new MeshGeometry3D(),
                        Material = new DiffuseMaterial(Brushes.Crimson)
                    };

                    MeshGeometry3D geometry = new MeshGeometry3D();
                    geometry.Positions.Add(new Point3D(x - 0.1, y - 0.1, 0.1));//0
                    geometry.Positions.Add(new Point3D(x - 0.1, y + 0.1, 0.1));//2
                    geometry.Positions.Add(new Point3D(x + 0.1, y - 0.1, 0.1));//1
                    geometry.Positions.Add(new Point3D(x + 0.1, y + 0.1, 0.1));//3
                    geometry.Positions.Add(new Point3D(x - 0.1, y - 0.1, 0.2));//4
                    geometry.Positions.Add(new Point3D(x - 0.1, y + 0.1, 0.2));//6
                    geometry.Positions.Add(new Point3D(x + 0.1, y - 0.1, 0.2));//5
                    geometry.Positions.Add(new Point3D(x + 0.1, y + 0.1, 0.2));//7

                    geometry.TriangleIndices.Add(6);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(7);

                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(3);

                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(3);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(5);
                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(6);
                    geometry.TriangleIndices.Add(7);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(7);
                    geometry.TriangleIndices.Add(5);

                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(1);
                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(5);

                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(1);
                    geometry.TriangleIndices.Add(3);

                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(1);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(0);
                    geometry.TriangleIndices.Add(2);

                    geometry.TriangleIndices.Add(4);
                    geometry.TriangleIndices.Add(2);
                    geometry.TriangleIndices.Add(6);

                    model.Geometry = geometry;
                    myModel.Children.Add(model);
                    drawnSubstations.Add(model);
                    models.Add(model, String.Format("ID: {0}\nNAME: {1}\nTYPE: SUBSTATION", s.Id, s.Name));
                }
            }
        }

        public void DrawLines()
        {
            double sLat = 0;
            double sLon = 0;
            Double x1 = 0;
            Double y1 = 0;
            Double x2 = 0;
            Double y2 = 0;

            double offsetLat = TopRightLat - BottomLeftLat;
            double offsetLon = TopRightLon - BottomLeftLon;

            bool firstFound = false;
            bool secondFound = false;

            Entity first;
            Entity second;

            int[] triangle = new int[] { 0, 1, 2 ,
                                    0, 2, 3 };

            foreach (var l in NetworkModel.Lines)
            {
                foreach (var e in EL)
                {
                    if (e.Id == l.FirstEnd)
                    {
                        firstFound = true;
                        first = e;
                    }

                    if (e.Id == l.SecondEnd)
                    {
                        secondFound = true;
                        second = e;
                    }

                    if (firstFound && secondFound)      // nece moci da formiraju liniju
                        break;
                }

                // pronadjene 2 tacke 
                if (firstFound && secondFound)
                {
                    for (int i = 0; i < l.Vertices.Count - 1; i++)
                    {
                        ToLatLon(l.Vertices[i].X, l.Vertices[i].Y, out sLat, out sLon);

                        if ((sLat > BottomLeftLat && sLat < TopRightLat) && (sLon < TopRightLon && sLon > BottomLeftLon))
                        {
                            y1 = -15 + (((sLat - BottomLeftLat) / offsetLat) * 30);
                            x1 = -15 + (((sLon - BottomLeftLon) / offsetLon) * 30);
                        }

                        ToLatLon(l.Vertices[i + 1].X, l.Vertices[i + 1].Y, out sLat, out sLon);

                        if ((sLat > BottomLeftLat && sLat < TopRightLat) && (sLon < TopRightLon && sLon > BottomLeftLon))
                        {
                            y2 = -15 + (((sLat - BottomLeftLat) / offsetLat) * 30);
                            x2 = -15 + (((sLon - BottomLeftLon) / offsetLon) * 30);
                        }

                        Point3D a1 = new Point3D(x1, y1, 0.02);
                        Point3D b1 = new Point3D(x2, y2, 0.02);

                        Vector3D v = b1 - a1;
                        Vector3D n = Vector3D.CrossProduct(v, new Vector3D(0, 0, 1));
                        n = Vector3D.Divide(n, n.Length);
                        n = Vector3D.Multiply(n, 0.01);

                        Point3D a11 = a1 + n;
                        Point3D a12 = a1 - n;
                        Point3D a21 = b1 + n;
                        Point3D a22 = b1 - n;

                        Point3D[] positions = new Point3D[] { a12, a11, a21, a22 };

                        GeometryModel3D model = new GeometryModel3D();
                        model.Geometry = new MeshGeometry3D();
                        model.Material = new DiffuseMaterial(Brushes.DarkGoldenrod);
                        MeshGeometry3D geometry = new MeshGeometry3D();

                        geometry.Positions = new Point3DCollection(positions);
                        geometry.TriangleIndices = new Int32Collection(triangle);

                        model.Geometry = geometry;
                        myModel.Children.Add(model);
                    }
                }
            }
        }

        public void GetLatLon()
        {
            double lon = 0;
            double lat = 0;

            foreach (SubstationEntity sse in NetworkModel.Substations)
            {
                sse.Latitude = lat;
                sse.Longitude = lon;
                SSL.Add(sse);
                EL.Add(sse);
            }
            foreach (SwitchEntity swe in NetworkModel.Switches)
            {
                ToLatLon(swe.X, swe.Y, out lat, out lon);
                swe.Latitude = lat;
                swe.Longitude = lon;
                SWL.Add(swe);
                EL.Add(swe);
            }
            foreach (NodeEntity ne in NetworkModel.Nodes)
            {
                ToLatLon(ne.X, ne.Y, out lat, out lon);
                ne.Latitude = lat;
                ne.Longitude = lon;
                NL.Add(ne);
                EL.Add(ne);
            }

        }

        public static void ToLatLon(double utmX, double utmY, out double latitude, out double longitude)
        {
            bool isNorthHemisphere = true;

            var diflat = -0.00066286966871111111111111111111111111;
            var diflon = -0.0003868060578;

            var zone = 34;
            var c_sa = 6378137.000000;
            var c_sb = 6356752.314245;
            var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
            var e2cuadrada = Math.Pow(e2, 2);
            var c = Math.Pow(c_sa, 2) / c_sb;
            var x = utmX - 500000;
            var y = isNorthHemisphere ? utmY : utmY - 10000000;

            var s = ((zone * 6.0) - 183.0);
            var lat = y / (c_sa * 0.9996);
            var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
            var a = x / v;
            var a1 = Math.Sin(2 * lat);
            var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
            var j2 = lat + (a1 / 2.0);
            var j4 = ((3 * j2) + a2) / 4.0;
            var j6 = ((5 * j4) + Math.Pow(a2 * (Math.Cos(lat)), 2)) / 3.0;
            var alfa = (3.0 / 4.0) * e2cuadrada;
            var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
            var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
            var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
            var b = (y - bm) / v;
            var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
            var eps = a * (1 - (epsi / 3.0));
            var nab = (b * (1 - epsi)) + lat;
            var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
            var delt = Math.Atan(senoheps / (Math.Cos(nab)));
            var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

            longitude = ((delt * (180.0 / Math.PI)) + s) + diflon;
            latitude = ((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat)) * (180.0 / Math.PI)) + diflat;
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            viewport1_MouseLeftButtonDown(viewport1, e);
        }

        private void viewport1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            viewport1.CaptureMouse();
            start = e.GetPosition(this);
            diffOffset.X = translacija.OffsetX;
            diffOffset.Y = translacija.OffsetY;

            if (e.MiddleButton == MouseButtonState.Pressed)
            {
                startPosition = e.GetPosition(this);
            }

            System.Windows.Point mouseposition = e.GetPosition(viewport1);
            curMouse = mouseposition;
            Point3D testpoint3D = new Point3D(mouseposition.X, mouseposition.Y, 0);
            Vector3D testdirection = new Vector3D(mouseposition.X, mouseposition.Y, 10);

            PointHitTestParameters pointparams =
                     new PointHitTestParameters(mouseposition);
            RayHitTestParameters rayparams =
                     new RayHitTestParameters(testpoint3D, testdirection);

            //test for a result in the Viewport3D     
            hitgeo = null;
            VisualTreeHelper.HitTest(viewport1, null, HTResult, pointparams);
        }

        private void viewport1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            viewport1.ReleaseMouseCapture();
        }

        private void viewport1_MouseMove(object sender, MouseEventArgs e)
        {
            txtTooltip.Text = "Entity's informations";

            if (e.MiddleButton == MouseButtonState.Pressed)
            {
                System.Windows.Point currentPosition = e.GetPosition(this);
                double offsetX = currentPosition.X - startPosition.X;
                double offsetY = currentPosition.Y - startPosition.Y;

                if ((xAngle.Angle + (1) * offsetY < 120 && xAngle.Angle + (1) * offsetY > -120))
                {
                    xAngle.Angle += (1) * offsetY;
                }
                if ((yAngle.Angle + (1) * offsetX < 120 && yAngle.Angle + (1) * offsetX > -120))
                {
                    yAngle.Angle += (1) * offsetX;
                }

                startPosition = currentPosition;
            }

            if (viewport1.IsMouseCaptured)
            {
                Point end = e.GetPosition(this);
                double offsetX = end.X - start.X;
                double offsetY = end.Y - start.Y;
                double w = this.Width;
                double h = this.Height;
                double translateX = (offsetX * 500) / w;
                double translateY = -(offsetY * 500) / h;
                translacija.OffsetX = diffOffset.X + (translateX / (10 * skaliranje.ScaleX));
                translacija.OffsetY = diffOffset.Y + (translateY / (10 * skaliranje.ScaleX));
            }
        }

        private void viewport1_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Point p = e.MouseDevice.GetPosition(this);
            double scaleX = 1;
            double scaleY = 1;

            if (e.Delta > 0 && zoomCurent < zoomMax)
            {
                scaleX = skaliranje.ScaleX + 0.1;
                scaleY = skaliranje.ScaleY + 0.1;
                zoomCurent++;
                skaliranje.ScaleX = scaleX;
                skaliranje.ScaleY = scaleY;
            }
            else if (e.Delta <= 0 && zoomCurent > -zoomMax)
            {
                scaleX = skaliranje.ScaleX - 0.1;
                scaleY = skaliranje.ScaleY - 0.1;
                zoomCurent--;
                skaliranje.ScaleX = scaleX;
                skaliranje.ScaleY = scaleY;
            }
        }

        private void viewport1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                return;
        }

        private HitTestResultBehavior HTResult(System.Windows.Media.HitTestResult rawresult)
        {
            //MessageBox.Show(rawresult.ToString());
            RayHitTestResult rayResult = rawresult as RayHitTestResult;

            if (rayResult != null)
            {
                bool gasit = false;

                for (int i = 0; i < models.Count; i++)
                {
                    foreach (KeyValuePair<GeometryModel3D, string> pair in models)
                    {
                        if (pair.Key == rayResult.ModelHit)
                        {
                            hitgeo = (GeometryModel3D)rayResult.ModelHit;
                            gasit = true;
                            Canvas.SetLeft(txtTooltip, curMouse.X + 12);
                            Canvas.SetTop(txtTooltip, curMouse.Y + 12);
                            txtTooltip.Text = pair.Value;
                        }
                    }
                }
                if (!gasit)
                {
                    hitgeo = null;
                }
            }

            return HitTestResultBehavior.Stop;
        }

        private void CheckBox1_Checked(object sender, RoutedEventArgs e)
        {
            foreach(var x in drawnNodes)
            {
                myModel.Children.Remove(x);
            }
        }

        private void CheckBox2_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var x in drawnSwitches)
            {
                myModel.Children.Remove(x);
            }
        }

        private void CheckBox3_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var x in drawnSubstations)
            {
                myModel.Children.Remove(x);
            }
        }

        private void CheckBox1_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var x in drawnNodes)
            {
                myModel.Children.Add(x);
            }
        }

        private void CheckBox2_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var x in drawnSwitches)
            {
                myModel.Children.Add(x);
            }
        }

        private void CheckBox3_Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var x in drawnSubstations)
            {
                myModel.Children.Add(x);
            }
        }
    }
}
